import org.junit.jupiter.api.Test;

import java.util.HashMap;

class PlaceCalculatorTest {

    @Test
    void test_getFinalPointsList() {
        HashMap<String, Integer> testCheck = new HashMap<String, Integer>();
        testCheck.put("John Smith", 4200);
        testCheck.put("Ben Linus", 4200);
        testCheck.put("Coo Coo", 3494);
        testCheck.put("Jaan Lepp",3494);
        testCheck.put("Jane Doe", 3199);
        testCheck.put("Foo Bar", 3099);

        PlaceCalculator placeCalculator = new PlaceCalculator("csvfiles/test_results.csv");
        assert placeCalculator.getFinalPointsList().equals(testCheck);
    }

    @Test
    void test_getFinishProtocol() {
        HashMap<String, String> testCheck = new HashMap<String, String>();
        testCheck.put("John Smith", "1-2");
        testCheck.put("Ben Linus", "1-2");
        testCheck.put("Coo Coo", "3-4");
        testCheck.put("Jaan Lepp", "3-4");
        testCheck.put("Jane Doe", "5");
        testCheck.put("Foo Bar", "6");

        PlaceCalculator placeCalculator = new PlaceCalculator("csvfiles/test_results.csv");
        assert placeCalculator.getFinishProtocol(placeCalculator.getFinalPointsList()).equals(testCheck);
    }
}