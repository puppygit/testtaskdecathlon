import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AthleteTest {

    @Test
    void test_setAthleteName() {
        Athlete athlete = new Athlete();
        athlete.setAthleteName("Jon Doe");

        assertEquals("Jon Doe", athlete.getAthleteName());
    }

    @Test
    void test_setRun100mResult() {
        Athlete athlete = new Athlete();
        athlete.setRun100mResult("9.58");

        assertEquals("9.58", athlete.getRun100mResult());
    }

    @Test
    void test_setLongJumpResult() {
        Athlete athlete = new Athlete();
        athlete.setLongJumpResult("7");

        assertEquals("7", athlete.getLongJumpResult());
    }

    @Test
    void test_setShotPutResult() {
        Athlete athlete = new Athlete();
        athlete.setShotPutResult("12");

        assertEquals("12", athlete.getShotPutResult());
    }

    @Test
    void test_setHighJumpResult() {
        Athlete athlete = new Athlete();
        athlete.setHighJumpResult("2.25");

        assertEquals("2.25",athlete.getHighJumpResult());
    }

    @Test
    void test_setRun400mResult() {
        Athlete athlete = new Athlete();
        athlete.setRun400mResult("43.31");

        assertEquals("43.31", athlete.getRun400mResult());
    }

    @Test
    void test_setRunHurdlesResult() {
        Athlete athlete = new Athlete();
        athlete.setRunHurdlesResult("11.12");

        assertEquals("11.12", athlete.getRunHurdlesResult());
    }

    @Test
    void test_setDiscusThrowResult() {
        Athlete athlete = new Athlete();
        athlete.setDiscusThrowResult("50");

        assertEquals("50", athlete.getDiscusThrowResult());
    }

    @Test
    void test_setPoleVaultResult() {
        Athlete athlete = new Athlete();
        athlete.setPoleVaultResult("6");

        assertEquals("6", athlete.getPoleVaultResult());
    }

    @Test
    void test_setJavelinThrowResult() {
        Athlete athlete = new Athlete();
        athlete.setJavelinThrowResult("70");

        assertEquals("70", athlete.getJavelinThrowResult());
    }

    @Test
    void test_setRun1500mResult() {
        Athlete athlete = new Athlete();
        athlete.setRun1500mResult("5.12.10");

        assertEquals("5.12.10", athlete.getRun1500mResult());
    }

    @Test
    void test_settPoints() {
        Athlete athlete = new Athlete();
        athlete.settPoints("Foo Bar", "csvfiles/test_results.csv");

        assertEquals(3099,athlete.gettPoints());
    }

    @Test
    void test_setAthletePoints() {
        Athlete athlete = new Athlete();
        athlete.setAthletePoints(10000);

        assertEquals(10000, athlete.getAthletePoints());
    }

    @Test
    void test_settPlace() {
        Athlete athlete = new Athlete();
        athlete.settPlace("Foo Bar", "csvfiles/test_results.csv");

        assertEquals("6", athlete.gettPlace());
    }

    @Test
    void test_setAthletePlace() {
        Athlete athlete = new Athlete();
        athlete.setAthletePlace("1");

        assertEquals("1", athlete.getAthletePlace());
    }
}