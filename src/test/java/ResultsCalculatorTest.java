import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ResultsCalculatorTest {

    /*
    For Benchmarks the official IAAF calculation tables for men(page 49):
    https://web.archive.org/web/20071203030644/http://www.iaaf.org/newsfiles/32097.pdf
     */
    @Test
    void test_setAthleteName() {
        ResultsCalculator athlete = new ResultsCalculator();
        athlete.setAthleteName("Sheldon Cooper");

        assertEquals("Sheldon Cooper", athlete.getAthleteName());
    }

    @Test
    void test_setRun100m() {
        ResultsCalculator athlete = new ResultsCalculator();
        athlete.setRun100m(10.40);

        assertEquals(999,(int) athlete.getRun100m());
    }

    @Test
    void test_setLongJump() {
        ResultsCalculator athlete = new ResultsCalculator();
        athlete.setLongJump(776);

        assertEquals(1000, (int)athlete.getLongJump());

    }

    @Test
    void test_setShotPut() {
        ResultsCalculator athlete = new ResultsCalculator();
        athlete.setShotPut(18.4);

        assertEquals(1000,(int)athlete.getShotPut());

    }

    @Test
    void test_setHighJump() {
        ResultsCalculator athlete = new ResultsCalculator();
        athlete.setHighJump(180);

        assertEquals(627,(int)athlete.getHighJump());
    }

    @Test
    void test_setRun400m() {
        ResultsCalculator athlete = new ResultsCalculator();
        athlete.setRun400m(46.17);

        assertEquals(1000,(int)athlete.getRun400m());

    }

    @Test
    void test_setRunHurdles() {
        ResultsCalculator athlete = new ResultsCalculator();
        athlete.setRunHurdles(13.8);

        assertEquals(1000,(int)athlete.getRunHurdles());

    }

    @Test
    void test_setDiscusThrow() {
        ResultsCalculator athlete = new ResultsCalculator();
        athlete.setDiscusThrow(56.17);

        assertEquals(1000,(int)athlete.getDiscusThrow());
    }

    @Test
    void test_setPoleVault() {
        ResultsCalculator athlete = new ResultsCalculator();
        athlete.setPoleVault(580);

        assertEquals(1165,(int)athlete.getPoleVault());
    }

    @Test
    void test_setJavelinThrow() {
        ResultsCalculator athlete = new ResultsCalculator();
        athlete.setJavelinThrow(77.19);

        assertEquals(1000,(int)athlete.getJavelinThrow());
    }

    @Test
    void test_setRun1500m() {
        ResultsCalculator athlete = new ResultsCalculator();

        final int SECONDS_IN_MINUTE = 60;
        String timeResult = "3.53.79";
        String[] splittedResult = timeResult.split("\\.",2);
        double timeInSeconds = Double.parseDouble(splittedResult[0])*SECONDS_IN_MINUTE + Double.parseDouble(splittedResult[1]);
        athlete.setRun1500m(timeInSeconds);

        assertEquals(1000,(int)athlete.getRun1500m());

    }

    @Test
    void test_getFinalResult() {
        ResultsCalculator athlete = new ResultsCalculator();
        athlete.setRun100m(10.40);
        athlete.setLongJump(776);
        athlete.setShotPut(18.4);
        athlete.setHighJump(180);
        athlete.setRun400m(46.17);
        athlete.setRunHurdles(13.8);
        athlete.setDiscusThrow(56.17);
        athlete.setPoleVault(580);
        athlete.setJavelinThrow(77.19);

        final int SECONDS_IN_MINUTE = 60;
        String timeResult = "3.53.79";
        String[] splittedResult = timeResult.split("\\.",2);
        double timeInSeconds = Double.parseDouble(splittedResult[0])*SECONDS_IN_MINUTE + Double.parseDouble(splittedResult[1]);
        athlete.setRun1500m(timeInSeconds);

        assertEquals(9791,athlete.getFinalResult());

    }
}