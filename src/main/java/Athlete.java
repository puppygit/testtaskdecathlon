import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@XmlType(propOrder = {"athleteName","athletePlace","athletePoints","run100mResult", "longJumpResult","shotPutResult",
        "highJumpResult", "run400mResult", "runHurdlesResult", "discusThrowResult","poleVaultResult", "javelinThrowResult", "run1500mResult"})
public class Athlete {

    private int athletePoints;
    private int tPoints;
    private String tPlace;
    private String athletePlace;
    private String athleteName;
    private String run100mResult;
    private String longJumpResult;
    private String shotPutResult;
    private String highJumpResult;
    private String run400mResult;
    private String runHurdlesResult;
    private String discusThrowResult;
    private String poleVaultResult;
    private String javelinThrowResult;
    private String run1500mResult;


    public Athlete(){
    }

    public String getAthleteName() {
        return athleteName;
    }

    @XmlAttribute(name = "Name")
    public void setAthleteName(String athleteName) {
        this.athleteName = athleteName;
    }

    public String getRun100mResult() {
        return run100mResult;
    }

    @XmlElement(name="Run 100m")
    public void setRun100mResult(String run100mResult) {
        this.run100mResult = run100mResult;
    }

    public String getLongJumpResult() {
        return longJumpResult;
    }

    @XmlElement(name="Long Jump")
    public void setLongJumpResult(String longJumpResult) {
        this.longJumpResult = longJumpResult;
    }

    public String getShotPutResult() {
        return shotPutResult;
    }

    @XmlElement(name="Shot Put")
    public void setShotPutResult(String shotPutResult) {
        this.shotPutResult = shotPutResult;
    }

    public String getHighJumpResult() {
        return highJumpResult;
    }

    @XmlElement(name="High Jump")
    public void setHighJumpResult(String highJumpResult) {
        this.highJumpResult = highJumpResult;
    }

    public String getRun400mResult() {
        return run400mResult;
    }

    @XmlElement(name="Run 400m")
    public void setRun400mResult(String run400mResult) {
        this.run400mResult = run400mResult;
    }

    public String getRunHurdlesResult() {
        return runHurdlesResult;
    }

    @XmlElement(name="Run 110m with Hurdles")
    public void setRunHurdlesResult(String runHurdlesResult) {
        this.runHurdlesResult = runHurdlesResult;
    }

    public String getDiscusThrowResult() {
        return discusThrowResult;
    }

    @XmlElement(name="Discus Throw")
    public void setDiscusThrowResult(String discusThrowResult) {
        this.discusThrowResult = discusThrowResult;
    }

    public String getPoleVaultResult() {
        return poleVaultResult;
    }

    @XmlElement(name="Pole Vault")
    public void setPoleVaultResult(String poleVaultResult) {
        this.poleVaultResult = poleVaultResult;
    }

    public String getJavelinThrowResult() {
        return javelinThrowResult;
    }

    @XmlElement(name="Javelin Throw")
    public void setJavelinThrowResult(String javelinThrowResult) {
        this.javelinThrowResult = javelinThrowResult;
    }

    public String getRun1500mResult() {
        return run1500mResult;
    }

    @XmlElement(name="Run 1500m")
    public void setRun1500mResult(String run1500mResult) {
        this.run1500mResult = run1500mResult;
    }

    public int gettPoints() {
        return tPoints;
    }

    public void settPoints(String athleteName, String csvFile) {
        PlaceCalculator placeCalculator = new PlaceCalculator(csvFile);
        HashMap<String,Integer> competitionResult = placeCalculator.getFinalPointsList();
        Object value = competitionResult.get(athleteName);
        this.tPoints = Integer.valueOf(value.toString());
    }

    public int getAthletePoints() {
        return athletePoints;
    }

    @XmlAttribute(name="Total Points")
    public void setAthletePoints(int athletePoints){
        this.athletePoints = athletePoints;
    }

    public String gettPlace(){
        return tPlace;
    }

    public void settPlace(String athleteName, String csvFile) {
        PlaceCalculator placeCalculator = new PlaceCalculator(csvFile);
        HashMap<String,String> competitionResult = placeCalculator.getFinishProtocol(placeCalculator.getFinalPointsList());
        Object value = competitionResult.get(athleteName);
        this.tPlace = String.valueOf(value);
    }

    public String getAthletePlace() {
        return athletePlace;
    }

    @XmlAttribute(name="Place")
    public void setAthletePlace(String athletePlace){
        this.athletePlace = athletePlace;
    }

}