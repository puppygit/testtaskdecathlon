import java.util.Comparator;

public class SortByPoints implements Comparator<Athlete> {
    public int compare(Athlete a, Athlete b) {
        return b.getAthletePoints() - a.getAthletePoints();
    }
}
