public class ResultsCalculator {

    private String athleteName;
    private double run100m;
    private double longJump;
    private double shotPut;
    private double highJump;
    private double run400m;
    private double runHurdles;
    private double discusThrow;
    private double poleVault;
    private double javelinThrow;
    private double run1500m;
    private int finalResult;

    /*
    multipliers for the formula
    Coefficients correspond to the Decathlon event in the following order:
    "[0]Run 100m", "[1]Long Jump", "[2]Shot Put", "[3]High Jump", "[4]Run 400m", "[5]Run 100m with Hurdles",
     "[6]Discus Throw", "[7]Pole vault", "[8]Javelin Throw", "[9]Run 1500m"
    */
    private double [] aMultiplier = new double[]{25.4347, 0.14354, 51.39, 0.8465, 1.53775, 5.74352, 12.91, 0.2797, 10.14, 0.03768};
    private double [] bMultiplier = new double[]{18, 220, 1.5, 75, 82, 28.5, 4, 100.00, 7, 480};
    private double [] cMultiplier = new double[]{1.81, 1.4, 1.05, 1.42, 1.81, 1.92, 1.1, 1.35, 1.08, 1.85};


    public ResultsCalculator(){
    }


    public String getAthleteName() {
        return athleteName;
    }

    public void setAthleteName(String athleteName) {
        this.athleteName = athleteName;
    }

    /*
    Different formulas have to be used for track and field events.
    Track events: Points = INT(A*(B-P)^C)
    Field events: Points = INT(A*(P-B)^C)
    Where:
    A - corresponding a_multiplier
    B - corresponding b_multiplier
    C - corresponding c_multiplier
    P - the result of the athlete for the event in corresponding units.
     */

    public double getRun100m() {
        return run100m;
    }

    public void setRun100m(double result100m){
        this.run100m = result100m;
        this.run100m = (int)(aMultiplier[0] * Math.pow(bMultiplier[0] - getRun100m(), cMultiplier[0]));
    }

    public double getLongJump() {
        return longJump;
    }

    public void setLongJump(double longJump) {
        this.longJump = longJump;
        this.longJump = (int)(aMultiplier[1] * Math.pow(getLongJump() - bMultiplier[1], cMultiplier[1]));
    }

    public double getShotPut() {
        return shotPut;
    }

    public void setShotPut(double shotPut) {
        this.shotPut = shotPut;
        this.shotPut = (int)(aMultiplier[2] * Math.pow(getShotPut() - bMultiplier[2], cMultiplier[2]));
    }

    public double getHighJump() {
        return highJump;
    }

    public void setHighJump(double highJump){
        this.highJump = highJump;
        this.highJump = (int)(aMultiplier[3] * Math.pow(getHighJump() - bMultiplier[3], cMultiplier[3]));
    }

    public double getRun400m() {
        return run400m;
    }

    public void setRun400m(double run400m){
        this.run400m = run400m;
        this.run400m = (int)(aMultiplier[4] * Math.pow(bMultiplier[4] - getRun400m(), cMultiplier[4]));
    }

    public double getRunHurdles() {
        return runHurdles;
    }

    public void setRunHurdles(double runHurdles){
        this.runHurdles = runHurdles;
        this.runHurdles = (int)(aMultiplier[5] * Math.pow(bMultiplier[5] - getRunHurdles(), cMultiplier[5]));
    }

    public double getDiscusThrow() {
        return discusThrow;
    }

    public void setDiscusThrow(double discusThrow) {
        this.discusThrow = discusThrow;
        this.discusThrow = (int)(aMultiplier[6] * Math.pow(getDiscusThrow() - bMultiplier[6], cMultiplier[6]));
    }

    public double getPoleVault() {
        return poleVault;
    }

    public void setPoleVault(double poleVault) {
        this.poleVault = poleVault;
        this.poleVault = (int)(aMultiplier[7] * Math.pow(getPoleVault() - bMultiplier[7], cMultiplier[7]));
    }

    public double getJavelinThrow() {
        return javelinThrow;
    }

    public void setJavelinThrow(double javelinThrow) {
        this.javelinThrow = javelinThrow;
        this.javelinThrow = (int)(aMultiplier[8] * Math.pow(getJavelinThrow() - bMultiplier[8], cMultiplier[8]));
    }

    public double getRun1500m() {
        return run1500m;
    }

    public void setRun1500m(double run1500m) {
        this.run1500m = run1500m;
        this.run1500m = (int)(aMultiplier[9] * Math.pow(bMultiplier[9] - getRun1500m(), cMultiplier[9]));
    }

    public int getFinalResult(){
        finalResult = (int)(getRun100m() + getLongJump() + getShotPut() + getHighJump() +
                + getRun400m() + getRunHurdles() + getDiscusThrow() + getPoleVault() + getJavelinThrow() + getRun1500m());
        return  finalResult;
    }
}