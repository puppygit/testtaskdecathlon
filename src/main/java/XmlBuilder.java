import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.namespace.QName;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class XmlBuilder {

    private String csvFile;

    public XmlBuilder(String csvFile){
        this.csvFile = csvFile;
    }

    public void generateXML(){

        AthleteList athleteList = new AthleteList();
        ArrayList<Athlete> athletes = new ArrayList<Athlete>();


        try{
            Scanner scanner = new Scanner(new File(csvFile));

            while (scanner.hasNextLine()){

                Athlete athlete = new Athlete();
                try{
                    String csvData = scanner.nextLine();
                    String[] results = csvData.split(";");

                    athlete.setAthleteName(results[0]);
                    athlete.settPlace(results[0],csvFile);
                    athlete.setAthletePlace(athlete.gettPlace());
                    athlete.setRun100mResult(results[1]);
                    athlete.setLongJumpResult(results[2]);
                    athlete.setShotPutResult(results[3]);
                    athlete.setHighJumpResult(results[4]);
                    athlete.setRun400mResult(results[5]);
                    athlete.setRunHurdlesResult(results[6]);
                    athlete.setDiscusThrowResult(results[7]);
                    athlete.setPoleVaultResult(results[8]);
                    athlete.setJavelinThrowResult(results[9]);
                    athlete.setRun1500mResult(results[10]);
                    athlete.settPoints(results[0],csvFile);
                    athlete.setAthletePoints(athlete.gettPoints());

                    athletes.add(athlete);

                } catch (ArrayIndexOutOfBoundsException e){
                    continue;
                }
            }
            athleteList.setList(athletes);
            Collections.sort(athletes, new SortByPoints());

            //generate XML out of athletes list.
            JAXBContext jaxbContext = JAXBContext.newInstance(AthleteList.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(athleteList,System.out);


        } catch (FileNotFoundException e){
            System.out.println("File " + e.getMessage().replace("(No such file or directory)", "") +
                    "not found! ");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
