import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class PlaceCalculator {

    static final private int SECONDS_IN_MINUTE = 60;
    static final private int CM_IN_METER = 100;

    private String filename;

    public PlaceCalculator(String csvFile){
        filename = csvFile;
    }


    public HashMap<String, Integer> getFinalPointsList() {

        HashMap<String, Integer> finalPointsList = new HashMap<String, Integer>();
        try {
            Scanner fileScanner = new Scanner(new File(filename));

            while (fileScanner.hasNextLine()){
                ResultsCalculator athleteResult = new ResultsCalculator();

                try{
                    String fileData = fileScanner.nextLine();
                    String points [] = fileData.split(";");

                    /*
                     * The following conversions should be done according to IAAF rules:
                     * Running results must be in seconds(Run100m, Run400m, RunHurdles, Run1500m).
                     * Throwing results must be in meters (ShotPush,DiscusThrow,JavelinThrow).
                     * Jumping events must be in centimeters(LongJump, HighJump, PoleVault).
                     */

                    athleteResult.setAthleteName(points[0]);
                    athleteResult.setRun100m(Double.parseDouble(points[1]));
                    athleteResult.setLongJump(Double.parseDouble(points[2])*CM_IN_METER);
                    athleteResult.setShotPut(Double.parseDouble(points[3]));
                    athleteResult.setHighJump(Double.parseDouble(points[4])*CM_IN_METER);
                    athleteResult.setRun400m(Double.parseDouble(points[5]));
                    athleteResult.setRunHurdles(Double.parseDouble(points[6]));
                    athleteResult.setDiscusThrow(Double.parseDouble(points[7]));
                    athleteResult.setPoleVault(Double.parseDouble(points[8])*CM_IN_METER);
                    athleteResult.setJavelinThrow(Double.parseDouble(points[9]));

                    String run1500mValue = points[10];
                    String[] run1500mTime = run1500mValue.split("\\.",2);
                    double run1500mInSeconds = Double.parseDouble(run1500mTime[0])*SECONDS_IN_MINUTE + Double.parseDouble(run1500mTime[1]);

                    athleteResult.setRun1500m(run1500mInSeconds);

                    finalPointsList.put(athleteResult.getAthleteName(),athleteResult.getFinalResult());

                } catch (ArrayIndexOutOfBoundsException e){
                }




            }
            fileScanner.close();

        } catch (FileNotFoundException e){
            System.out.println("File " + e.getMessage().replace("(No such file or directory)", "") +
                    "not found! ");
        }
        return sortByComparator(finalPointsList);
    }


    private static HashMap<String, Integer> sortByComparator(HashMap<String, Integer> unsortMap) {

        List<HashMap.Entry<String, Integer>> list = new LinkedList<HashMap.Entry<String, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<HashMap.Entry<String, Integer>>() {
            public int compare(HashMap.Entry<String, Integer> o1, HashMap.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        // Maintaining insertion order with the help of LinkedList
        HashMap<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (HashMap.Entry<String, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    /**
     * Method for calculation of the athlete places based on the total points.
     * @param sortedMap - sorted HashMap returned by generateFinalPointsList();
     * @return returns HashMap with the Athlete Name as Key and place as Value.
     */
    public HashMap<String,String> getFinishProtocol(HashMap<String, Integer> sortedMap){

        List<Map.Entry<String,Integer>> list = new ArrayList<Map.Entry<String, Integer>>(sortedMap.entrySet());
        String [] keys = new String[list.size()];
        String [] values = new String[list.size()];
        String [] new_keys = new String [list.size()];

        for (int i = 0; i<list.size(); i++){
            Map.Entry<String,Integer> e = list.get(i);
            keys[i] = String.valueOf(e.getKey());
            values[i] = String.valueOf(e.getValue());

        }

        for (int i = 0; i<values.length; i++){
            if (i==0){
                new_keys[i] = String.valueOf(i+1);
            } else if(values[i].equals(values[i-1])){
                new_keys[i] = String.valueOf(i)+"-"+String.valueOf(i+1);
                new_keys[i-1] = new_keys[i];
            } else {
                new_keys[i]=String.valueOf(i+1);
            }
        }
        HashMap finishProtocol = new LinkedHashMap();
        for (int j=0; j<new_keys.length; j++){
            finishProtocol.put(keys[j],new_keys[j]);
        }
        return finishProtocol;

    }


}
