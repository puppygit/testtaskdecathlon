import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "athletes")
public class AthleteList {

    @XmlElement(name="athlete")
    List<Athlete> athletes = new ArrayList<Athlete>();

    public AthleteList(){ }

    public void setList(List<Athlete> athletes){
        this.athletes = athletes;
    }
}
